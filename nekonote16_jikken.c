/*
jiki
2015/12/13
ライントレース
*/
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include "stdlib.h"

#define COLOR_SENSOR NXT_PORT_S3
#define L_MOTOR NXT_PORT_B
#define R_MOTOR NXT_PORT_C
#define L_LIGHT NXT_PORT_S1
#define R_LIGHT NXT_PORT_S2

#define BLACK_VAL 660
#define WHITE_VAL 400
#define GRAY_VAL 500
#define BASE_SPEED 40
#define P_GAIN 0.39
#define D_GAIN 10.0

DeclareCounter(SysTimerCnt);
DeclareTask(Task1);

void reset_rotate_angle();//rotate_angleなどのリセット
void find_line_by_light(int n);//pd制御からの曲がり角検知
void turn_corner(int mode,int angle);//左右回転
int end_turn_by_angle(int mode,int angle);//左右回転の終了角度
void tyousei();//曲がり角回転前の調整
void run_pid();//直線のpd制御

int color_ave();
int what_is_color();

static int left_speed,right_speed;
static int right_turn,left_turn;
static int state=0;
static int r_right,l_right;
static int l_rotate_angle,r_rotate_angle;
static S8 count=0,l;
static int online;
static int r_right_tmp=0,l_light_tmp=0;
static int n0_r_right,n0_l_right,n1_r_right,n1_l_right;
static int n=0,m;
static int n_l_light[100],n_r_light[100];

S16 rgb[3];
S32 rgb_ave[3];


void ecrobot_device_initialize(){
	ecrobot_init_bt_slave("LEJOS-OSEK");
	ecrobot_set_light_sensor_active(L_LIGHT);
	ecrobot_set_light_sensor_active(R_LIGHT);
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	nxt_motor_set_speed(NXT_PORT_A,0,0);
	ecrobot_init_nxtcolorsensor(COLOR_SENSOR, NXT_COLORSENSOR);
	state=0;
}		//OSEK起動時の処理

void ecrobot_device_terminate(){
	ecrobot_set_light_sensor_inactive(L_LIGHT);
	ecrobot_set_light_sensor_inactive(R_LIGHT);
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	ecrobot_term_bt_connection();
	nxt_motor_set_speed(NXT_PORT_A,0,0);
}		//OSEK終了時の処理

void user_1ms_isr_type2(void){
	StatusType ercd;

	ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */
	if (ercd != E_OK)
	{
		ShutdownOS(ercd);
	}
}

TASK(Task1)
{	
	static int arm_angle;	
	
	static float x,y;
	
	static S8 i,j;
	
	static int kensa=0;
	
	static int color=3;
	
	//カラーセンサー用
	ecrobot_process_bg_nxtcolorsensor();
	ecrobot_set_nxtcolorsensor(COLOR_SENSOR,NXT_COLORSENSOR);//動作モード設定
	
	ecrobot_get_nxtcolorsensor_rgb(COLOR_SENSOR, rgb);
		
	
	n0_r_right = n1_r_right;
	n0_l_right = n1_l_right;

	n1_r_right = ecrobot_get_light_sensor(R_LIGHT)+20;
	n1_l_right = ecrobot_get_light_sensor(L_LIGHT);//調整とかする？
	
	
	
	r_right=(n0_r_right+n1_r_right)/2;
	l_right=(n0_l_right+n1_l_right)/2;
	
	right_turn = P_GAIN * (r_right - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) 
	     + D_GAIN * (r_right-r_right_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
		 
	left_turn = P_GAIN * (l_right - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) 
	     + D_GAIN * (l_right-l_light_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
		 
	r_right_tmp=r_right;
	l_light_tmp=l_right;
	
	n_r_light[i%100]=r_right;
	n_l_light[i%100]=l_right;
	
	if(state==0){
		run_pid(99);
		if(nxt_motor_get_count(NXT_PORT_A) > -35){
			nxt_motor_set_speed(NXT_PORT_A,-40,1);
		}else {
			reset_rotate_angle();
			state=1;
		}
	}
	if(state==1){
		turn_corner(2,800);
		end_turn_by_angle(2,800);
		x=color_ave();
	}
	if(state==2){
		run_pid(99);
		color=what_is_color();
		state=3;
	}
	if(state==3){
		run_pid(99);
	}
	
	
	
	// //----------------------------------
	// //test
	// //pd
	// if(state==101){
	// 	run_pid(1);
	// 	find_line_by_light(0);
	// }
	// if(state==102){
	// 	tyousei();
	// }
	// //右90回転
	// if(state==103){
	// 	turn_corner(1,120);
	// }
	// if(state==104){
	// 	run_pid(0);
	// 	find_line_by_light(1);
	// }
	// //----------------------------------
	// //----------------------------------
	// //test
	// //pd
	// if(state==111){
	// 	run_pid(1);
	// 	find_line_by_light(0);
	// }
	// if(state==112){
	// 	tyousei();
	// }
	// //右90回転
	// if(state==113){
	// 	turn_corner(2,115);
	// }
	// if(state==114){
	// 	run_pid(1);
	// 	find_line_by_light(0);
	// }
	// //----------------------------------
	// //----------------------------------
	// //test
	// if(state==121){
	// 	run_pid(0);
	// 	find_line_by_light(1);
	// }
	// if(state==122){
	// 	tyousei();
	// }
	// //左90回転
	// if(state==123){
	// 	turn_corner(3,120);
	// }
	// if(state==124){
	// 	run_pid(1);
	// 	find_line_by_light(0);
	// }
	// //----------------------------------
	// //----------------------------------
	// //test
	// if(state==131){
	// 	run_pid(0);
	// 	find_line_by_light(1);
	// }
	// if(state==132){
	// 	tyousei();
	// }
	// //左90回転
	// if(state==133){
	// 	turn_corner(4,115);
	// }
	// if(state==134){
	// 	run_pid(0);
	// 	find_line_by_light(1);
	// }
	// //----------------------------------
	


	display_goto_xy(0, 2);
	display_string("R:");
	display_int(rgb[0],0);
	display_goto_xy(0, 3);
	display_string("G:");
	display_int(rgb[1],0);
	display_goto_xy(0, 4);
	display_string("B:");
	display_int(rgb[2],0);
	display_update();
	


	// display_goto_xy(0, 1);
	// display_string("i am=");
	// display_int(state,0);
	// display_goto_xy(0, 2);
	// display_string("mode=");
	// display_int(mode,0);
	
	
	
	display_goto_xy(0, 1);
	display_string("state=");
	display_int(state,0);
	display_goto_xy(0, 2);
	display_string("rgb_ave[0]=");
	display_int(rgb_ave[0],0);
	display_goto_xy(0, 3);
	display_string("rgb_ave[1]=");
	display_int(rgb_ave[1],0);
	display_goto_xy(0, 4);
	display_string("rgb_ave[2]=");
	display_int(rgb_ave[2],0);
	display_goto_xy(0, 5);
	display_string("color=");
	display_int(color,0);
	

	display_update();

	
	ecrobot_bt_data_logger(state, online);//ログ収集
	if(i==100)i=0;

	TerminateTask();
}

//rgbの100個分のデータをrgb_aveに入れる
int color_ave(){
	static int count=1;
	if(count<100){
		rgb_ave[0]+=rgb[0];
		rgb_ave[1]+=rgb[1];
		rgb_ave[2]+=rgb[2];
		count++;
	}
	return 0;
}

//色の判別
int what_is_color(){
	int n;
	int max,max_n;
	int color2[3];
	int ave_3color;
	int bunsan;
	
	//大きめの数字で割ることで情報量を減らす
	color2[0]=rgb_ave[0]/1000;
	color2[1]=rgb_ave[1]/1000;
	color2[2]=rgb_ave[2]/1000;
	
	//分散値の計算
	ave_3color=(color2[0]+color2[1]+color2[2])/3;
	bunsan=(((color2[0]-ave_3color)*(color2[0]-ave_3color)) + ((color2[1]-ave_3color)*(color2[1]-ave_3color)) + ((color2[2]-ave_3color)*(color2[2]-ave_3color)))/3;
	
	display_goto_xy(0, 6);
	display_string("bunsan=");
	display_int(bunsan,0);
	display_goto_xy(0, 7);
	display_string("ave_3color=");
	display_int(ave_3color,0);
	
	//最も値が大きかったものをそこにある色として、max_nに格納
	//red:0 green:1 blue:2
	max=rgb_ave[0];
	max_n=0;
	for(n=1;n<3;n++){
		if(max<rgb_ave[n]){
			max=rgb_ave[n];
			max_n=n;
		}
	}
	return max_n;	
}

void reset_rotate_angle(){
	l_rotate_angle=nxt_motor_get_count(L_MOTOR);
	r_rotate_angle=nxt_motor_get_count(R_MOTOR);
	count=0;
	l=0;
	online=0;
}
void run_pid(int mode){
	//右でよむ---------------------------------------------------------------
	
	if(mode==0){
		if(n_r_light[m]>GRAY_VAL){
			if(count<100){
				count+=5;
			}else count=100;
		}else{
			if(count>-100){
				count--;
			}else count=-100;
		}
		if(online==2 && count>0){
			online=2;
		}else if(count>0 && nxt_motor_get_count(R_MOTOR) > r_rotate_angle+100){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>40){
				online=2;
				l=0;
			}
		}else{
			l=0;	
		}
		
		
		left_speed=BASE_SPEED+right_turn;
		right_speed=BASE_SPEED-right_turn;
		
		if(online==0||online==1){
			left_speed+=(right_turn)/2;
			right_speed-=(right_turn)/2;
		}
		
		if(online==2 && state!=3 && state!=33 && state!=38){
			left_speed+=40;
			right_speed+=40;
			l=0;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	
	//左で読む---------------------------------------------------------------
	
	if(mode==1){
		if(n_l_light[m]>GRAY_VAL){
			if(count<100){
				count+=5;
			}else count=100;
		}else{
			if(count>-100){
				count--;
			}else count=-100;
		}
		
		if(online==2 && count>0){
			online=2;
		}else if(count>0 && nxt_motor_get_count(L_MOTOR) > l_rotate_angle+100){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>40){
				online=2;
				l=0;
			}
		}else{
			l=0;
		}
		
	
		left_speed=BASE_SPEED-left_turn;
		right_speed=BASE_SPEED+left_turn;
		
		if(online==0||online==1){
			left_speed-=(left_turn)/2;
			right_speed+=(left_turn)/2;
		}
		
		
		if(online==2 && state!=3){
			left_speed+=40;
			right_speed+=40;
			l=0;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	
	//右でよむバック---------------------------------------------------------------
	
	if(mode==10){
		if(n_r_light[m]>GRAY_VAL){
			if(count<100){
				count+=5;
			}else count=100;
		}else{
			if(count>-100){
				count--;
			}else count=-100;
		}
		if(online==2 && count>0){
			online=2;
		}else if(count>0 && nxt_motor_get_count(R_MOTOR) > r_rotate_angle+100){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>40){
				online=2;
				l=0;
			}
		}else{
			l=0;	
		}
		
		
		left_speed=BASE_SPEED+right_turn;
		right_speed=BASE_SPEED-right_turn;
		
		if(online==0||online==1){
			left_speed+=(right_turn)/2;
			right_speed-=(right_turn)/2;
		}
		
		if(online==2){
			left_speed+=40;
			right_speed+=40;
			l=0;
		}
		//ひっくり返す
		left_speed*=(-1);
		right_speed*=(-1);
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	//右でよむXXX---------------------------------------------------------------
	
	if(mode==10){
		right_turn = 0.6 * (r_right - (GRAY_VAL-50))*100 / (BLACK_VAL-150 - WHITE_VAL) 
	     + D_GAIN * (r_right-r_right_tmp)/(float)((BLACK_VAL-150) - WHITE_VAL )*100;
		 
		left_turn = 0.6 * (l_right - (GRAY_VAL-50))*100 / (BLACK_VAL-150 - WHITE_VAL) 
	     + D_GAIN * (l_right-l_light_tmp)/(float)((BLACK_VAL-150) - WHITE_VAL )*100;
				
		online=0;
		
		left_speed=BASE_SPEED+right_turn;
		right_speed=BASE_SPEED-right_turn;
		
		if(online==0){
			left_speed+=(right_turn)/2;
			right_speed-=(right_turn)/2;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	if(mode==99){
		nxt_motor_set_speed(L_MOTOR,0,0);
		nxt_motor_set_speed(R_MOTOR,0,0);
	}
}

//引数 0:right 1:left
void find_line_by_light(int n){
	if(n==0){
		if(r_right>GRAY_VAL+20 
		&& nxt_motor_get_count(L_MOTOR) > l_rotate_angle+200){
			reset_rotate_angle();
			state+=1;
		}
	}else if(n==1){
		if(l_right>GRAY_VAL+20 
		&& nxt_motor_get_count(R_MOTOR) > r_rotate_angle+200){
			reset_rotate_angle();
			state+=1;
		}
	}
}
void tyousei(){
	if(online==0||online==1){
		nxt_motor_set_speed(L_MOTOR,40,1);
		nxt_motor_set_speed(R_MOTOR,40,1);
		if(nxt_motor_get_count(L_MOTOR)>l_rotate_angle+15){
			state+=1;
			reset_rotate_angle();
		}
	}else if (online==2){
		nxt_motor_set_speed(L_MOTOR,-30,1);
		nxt_motor_set_speed(R_MOTOR,-30,1);
		  if(nxt_motor_get_count(L_MOTOR)<l_rotate_angle-15){
			state+=1;
			reset_rotate_angle();
		  }
	}
}

void turn_corner(int mode,int angle){
	static int status=0;
	switch(mode){
		//右回転次のセンサ違うとき
		case 1:
			if(status==0){
				nxt_motor_set_speed(L_MOTOR,-40,1);
				nxt_motor_set_speed(R_MOTOR,-40,1);
				if(nxt_motor_get_count(L_MOTOR) < l_rotate_angle-50){
					status=1;
				}
			}else{
				nxt_motor_set_speed(L_MOTOR,40,1);
				nxt_motor_set_speed(R_MOTOR,-45,1);
				status=end_turn_by_angle(mode,angle);
			}
			break;
		//右回転次のセンサ同じとき
		case 2:
			nxt_motor_set_speed(L_MOTOR,40,1);
			nxt_motor_set_speed(R_MOTOR,-40,1);
			status=end_turn_by_angle(mode,angle);
			break;
		//左回転次のセンサ違うとき
		case 3:
			if(status==0){
				nxt_motor_set_speed(L_MOTOR,-40,1);
				nxt_motor_set_speed(R_MOTOR,-40,1);
				if(nxt_motor_get_count(R_MOTOR) < r_rotate_angle-50){
					status=1;
				}
			}else{
				nxt_motor_set_speed(L_MOTOR,-45,1);
				nxt_motor_set_speed(R_MOTOR,40,1);
				status=end_turn_by_angle(mode,angle);
			}
			break;
		//左回転次のセンサ同じとき
		case 4:
			nxt_motor_set_speed(L_MOTOR,-40,1);
			nxt_motor_set_speed(R_MOTOR,40,1);
			status=end_turn_by_angle(mode,angle);
			break;
		//180回転
		case 5:
			nxt_motor_set_speed(L_MOTOR,40,1);
			nxt_motor_set_speed(R_MOTOR,-45,1);
			if(nxt_motor_get_count(L_MOTOR) > l_rotate_angle+120 && nxt_motor_get_count(L_MOTOR) < l_rotate_angle+140){
				nxt_motor_set_speed(L_MOTOR,40,1);
				nxt_motor_set_speed(R_MOTOR,40,1);
			}
			status=end_turn_by_angle(mode,angle);
			break;
		case 6:
			
			break;
	}
}
 int end_turn_by_angle(int mode,int angle){
	switch(mode){
		//右回転
		case 1:
		case 2:
		case 5:
			if(nxt_motor_get_count(L_MOTOR) > l_rotate_angle + angle){
				reset_rotate_angle();
				state+=1;
				return 0;
			}
			break;
		//左回転
		case 3:
		case 4:
			if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle + angle){
				reset_rotate_angle();
				state+=1;
				return 0;
			}
			break;
	}
	
}


