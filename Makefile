# Target specific macros
TARGET = nekonote16_jikken
NXTOSEK_ROOT = /usr/local/nxtOSEK
TARGET_SOURCES = \
	nekonote16_jikken.c
TOPPERS_OSEK_OIL_SOURCE = ./nekonote16_jikken.oil

# Don't modify below part
O_PATH ?= build
include ../../ecrobot/ecrobot.mak
